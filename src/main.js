import Vue from 'vue'
import App from './App'
import api from './api'
import path from './common/path/path'
import utils from './common/utils/utils'
import tool from './common/tool/tool'

Vue.config.productionTip = false
Vue.prototype.$path = path
Vue.prototype.$utils = utils
Vue.prototype.$tool = tool

Vue.prototype.$T = 200 // 返回成功的标记
Vue.prototype.$ratio = 1.5 // => 宽/长

Vue.prototype.$tips = {
	'uploadQiniu': '上传失败，请重试',  // 七牛
	'uploadService': '上传失败，请重试',  // 服务端
	'uploadSuccess': '上传成功，请等待'
}


App.mpType = 'app'

const app = new Vue({
	...App
})

Vue.use(api)
app.$mount()
