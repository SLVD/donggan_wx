import fetch from '../common/fetch'

// 系统数据

const oUrl = {


	templateList: '/movie/template/list/',

	templateTag: '/movie/template/cat/',

	templateDetail: '/movie/template/detail/',

	uploadImg: '/movie/create/',

	movieList: '/movie/list/',


}


export function templateList (params, data) {
	return fetch({
		url: oUrl.templateList,
		method: "GET",
		params,
		data
	})
}


export function templateTag (params, data) {
	return fetch({
		url: oUrl.templateTag,
		method: "GET",
		params,
		data
	})
}


export function templateDetail (params, data) {
	return fetch({
		url: oUrl.templateDetail,
		method: "GET",
		params,
		data
	})
}


export function uploadImg (params, data) {
	return fetch({
		url: oUrl.uploadImg,
		method: "POST",
		params,
		data
	})
}


export function movieList (params, data) {
	return fetch({
		url: oUrl.movieList,
		method: "GET",
		params,
		data
	})
}