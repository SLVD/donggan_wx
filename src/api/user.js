import fetch from '../common/fetch'

// 系统数据

const oUrl = {

  qiniuToken: 'user/qiniuToken/',

  getLogin: 'user/login/',

  pay: '/user/pay/',
  

  // ======

  getCareData: 'user/focus/list/', // 关注列表

  getBeanData: 'user/beFocus/list/', // 粉丝列表

  careUser: 'user/focus/on/', // 关注

  cancelCareUser: 'user/focus/cancle/', // 取消关注

}

export function getQiniuToken(params) {
    return fetch({
      url: oUrl.qiniuToken,
      method: 'GET',
      params
    })
  }


export function getLogin(params, data) {
  return fetch({
    url: oUrl.getLogin,
    method: "POST",
    params,
    data
  })
}


export function pay(params, data) {
  return fetch({
    url: oUrl.pay,
    method: "POST",
    params,
    data
  })
}


export function getCareData(params) {
  return fetch({
    url: oUrl.getCareData,
    method: 'GET',
    params
  })
}


export function getBeanData(params) {
  return fetch({
    url: oUrl.getBeanData,
    method: 'GET',
    params
  })
}


export function careUser(params, data) {
  return fetch({
    url: oUrl.careUser,
    method: 'POST',
    params,
    data
  })
}


export function cancelCareUser(params, data) {
  return fetch({
    url: oUrl.cancelCareUser,
    method: 'DELETE',
    params,
    data
  })
}