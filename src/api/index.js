
import * as apiTest from './test'
import * as apiMovie from './movie'
import * as apiSystem from './system'
import * as apiUser from './user'
import * as apiAlbum from './album'
import * as apiSetting from './setting'

const apiObj = {
    apiMovie,

    apiSystem,
    apiUser,
    apiAlbum,
    apiSetting,
    apiTest,
};

const install = function (Vue) {
    if (install.installed) return;
    install.installed = true;
    Object.defineProperties(Vue.prototype, {
        $fetch: {
            get() {
                return apiObj
            }
        }
    })
};

export default {
    install
}
