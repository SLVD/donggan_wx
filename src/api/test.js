import fetch from '../common/fetch'

// 系统数据

const oUrl = {
	
	getTestData: 'test/',  //
	qiniuToken: 'system/qiniuToken/', // http://127.0.0.1:8000/system/qiniuToken/
	
}


export function getTestData(params) {
	return fetch({
		url: oUrl.getTestData,
		method: "GET",
		params
	})
}


export function getQiniuToken(params) {
    return fetch({
      url: oUrl.qiniuToken,
      method: 'GET',
      params
    })
  }