import fetch from '../common/fetch'

// 系统数据

const oUrl = {

	getTemplateTag: 'album/template/list/', // 模板列表

	getAlbumTag: 'album/tag/list/', // 主页标签列表

	getAlbumList: 'album/list/', // 影集列表

	getUserMusicData: 'album/music/list/', // 用户音乐列表

	getSearchMusicItem: 'album/template/music/tag/list/', // 模板音乐分类列表

	getSearchMusicTemplate: 'album/template/music/list/', // 模板音乐列表

	getUserPhotoAlbum: 'album/photograph/list/', // 用户照片列表
}


export function getTemplateTag(params) {
	return fetch({
		url: oUrl.getTemplateTag,
		method: "GET",
		params
	})
}


export function getAlbumTag(params) {
	return fetch({
		url: oUrl.getAlbumTag,
		method: "GET",
		params
	})
}


export function getAlbumList(params) {
	return fetch({
		url: oUrl.getAlbumList,
		method: "GET",
		params
	})
}


export function getUserMusicData(params) {
	return fetch({
		url: oUrl.getUserMusicData,
		method: 'GET',
		params
	})
}


export function getSearchMusicItem(params) {
	return fetch({
		url: oUrl.getSearchMusicItem,
		method: 'GET',
		params
	})
}


export function getSearchMusicTemplate(params) {
	return fetch({
		url: oUrl.getSearchMusicTemplate,
		method: 'GET',
		params
	})
}


export function getUserPhotoAlbum(params) {
	return fetch({
		url: oUrl.getUserPhotoAlbum,
		method: 'GET',
		params
	})
}