import fetch from '../common/fetch'

// 系统数据

const oUrl = {

	getAccessToken: 'url: https://api.weixin.qq.com/cgi-bin/token',

	getTestData: 'test/',  //

	qiniuToken: 'system/qiniuToken/', // http://127.0.0.1:8000/system/qiniuToken/

	getFAQ: 'setting/faq/detail/', // FAQ

	getGuide: 'setting/guide/detail/', // 教程指引


	serviceNotice: '',

}


export function getAccessToken(params) {
	return fetch({
		url: oUrl.getAccessToken,
		method: "GET",
		params
	})
}


export function getTestData(params) {
	return fetch({
		url: oUrl.getTestData,
		method: "GET",
		params
	})
}


export function getQiniuToken(params) {
	return fetch({
		url: oUrl.qiniuToken,
		method: 'GET',
		params
	})
}


export function getFAQ(params) {
	return fetch({
		url: oUrl.getFAQ,
		method: 'GET',
		params
	})
}


export function getGuide(params) {
	return fetch({
		url: oUrl.getGuide,
		method: 'GET',
		params
	})
}