import service from '../../common/service/service';
const jweixin = require('jweixin-module')
export default {
    data () {
        return {
            work_index: 0,
            autoplay: false,
            tempWidth: 0, // 模板宽度
            tempHeight: 0, // 模板高度
            percent: 0, // 下载进度
            count: 0, // 数据总数
            movieParam: {
                offset: 0,
                limit: 10
            },
            test_url: 'https://qiniu.digfunny.com/0c2946e4-b43d-11e9-bd46-d46d6dce8767-76cbb846d6161d95c50b2bd116177d05.mp4',
            // test_url: 'http://qiniu.digfunny.com/media/video/show/%E6%9C%80%E7%BB%88%E6%B8%B2%E6%9F%93_x264.mp4',
            mine: {
                portrait: 'https://wx.qlogo.cn/mmopen/vi_32/RBrrTnljDWHAr0VKmOicD0l4VQnuvj76HArWZib3C5jINBR9l9wIia2Cm1iayI0Cy7UMUG0YxFC4S1fZ8hVib6DFqZg/132',
                name: '走',
                name_id: '909090'
            },
            username: '请点击登录',
            imgUrl: '../../static/icon/head.png',
            vlogList: [], // 作品列表,
            loadingText: ['加载中...', '已加载全部'], // 当前状态
            loadingStatus: -1 // -1, 0, 1
        }
    },
    onLoad () {
        const userInfo = uni.getStorageSync('userInfo')
        if (!this.$utils.isEmpty(userInfo)) {
            this.imgUrl = userInfo.avatarUrl
            this.username = userInfo.nickName
        }
    },
    onShow () {
        this.initData()
        const { windowWidth, windowHeight } = uni.getSystemInfoSync();
        this.tempWidth = (windowWidth - 30) / 2
        this.tempHeight = this.tempWidth * this.$ratio
        console.log(windowWidth, windowHeight)
        console.log(this.tempWidth, this.tempHeight)

        console.log('service', service)
        this.initProvider()

        let token = uni.getStorageSync('token')
        console.log('token', token)

        console.log('ssss', jweixin)
        wx.checkJsApi({
            jsApiList: ['chooseImage'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
            success: function (res) {
                console.log('cc', res)
                // 以键值对的形式返回，可用的api值true，不可用为false
                // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
            }
        });

    },
    onPullDownRefresh () {
        this.initData()
        setTimeout(() => {
            uni.stopPullDownRefresh();
        }, 1000);
    },
    onReachBottom () {
        if (this.movieParam.offset * this.movieParam.limit < this.count) {
            this.movieParam.offset++
            this.loadingStatus = 0
            this.getMovieList()
        } else {
            this.loadingStatus = 1
            setTimeout(() => {
                this.loadingStatus = -1
            }, 1e3);
        }
    },
    methods: {
        initProvider () {
            const filters = ['weixin', 'qq', 'sinaweibo'];
            uni.getProvider({
                service: 'oauth',
                success: (res) => {
                    console.log('provider', res)
                    uni.login({
                        provider: res.service,
                        success: (res) => {
                            uni.getUserInfo({
                                provider: res.service,
                                success: (infoRes) => {
                                    /**
                                     * 实际开发中，获取用户信息后，需要将信息上报至服务端。
                                     * 服务端可以用 userInfo.openId 作为用户的唯一标识新增或绑定用户信息。
                                     */
                                    // this.toMain(infoRes.userInfo.nickName);
                                    console.log('infoRes')
                                },
                                fail () {
                                    uni.showToast({
                                        icon: 'none',
                                        title: '登陆失败'
                                    });
                                }
                            });
                        },
                        fail: (err) => {
                            console.error('授权登录失败：' + JSON.stringify(err));
                        }
                    });
                },
                fail: (err) => {
                    console.error('获取服务供应商失败：' + JSON.stringify(err));
                }
            });
        },
        initData () {
            if (uni.getStorageSync('token') !== '') {
                this.vlogList = []
                this.getMovieList()
            }
        },
        handleDownload (url) {
            const downloadTask = uni.downloadFile({
                url: url, //仅为示例，并非真实的资源
                header: {
                    "Content-Type": "video/mpeg4",
                },
                success: (res) => {
                    if (res.statusCode === 200) {
                        // console.log('下载成功', res);
                        uni.saveVideoToPhotosAlbum({
                            filePath: res.tempFilePath,
                            success: (e) => {
                                // console.log('下载成功', e);
                                console.log('save success', wx.env.USER_DATA_PATH);
                            },
                            fail: (e) => {
                                console.error('upload', e)
                            },
                            complete: (e) => {
                                // console.log('complete', e)
                            }
                        });
                    }
                }
            });

            downloadTask.onProgressUpdate((res) => {
                // console.log('下载进度' + res.progress);
                // console.log('已经下载的数据长度' + res.totalBytesWritten);
                // console.log('预期需要下载的数据总长度' + res.totalBytesExpectedToWrite);
                this.percent = res.progress
                if (res.progress == 100) {
                    setTimeout(() => {
                        this.percent = 0
                        uni.showToast({
                            title: '下载成功',
                        })
                    }, 3000);
                }
            });
        },
        getMovieList () {
            uni.showLoading({
                title: '加载中'
            })
            this.$fetch.apiMovie.movieList({}, this.movieParam)
                .then(res => {
                    uni.hideLoading()
                    this.loadingStatus = -1
                    // console.log('movie list', res)
                    const data = res.data
                    // console.log('data data', data)
                    if (data.status === this.$T) {
                        // console.log('list list', data.data.elements)
                        // console.log('list fields', data.data.fields)
                        this.count = data.data.fields
                        this.vlogList = this.vlogList.concat(data.data.elements)
                    }
                    setTimeout(() => {
                    }, 3e3);
                })
                .catch(err => {
                    uni.hideLoading()
                    this.loadingStatus = -1
                    console.error(err)
                })
        },
        handleSwitch (index) {
            if (this.work_index != index) {
                this.work_index = index
            }
        },
        handleConfirmLogin (e) {
            if (e.flag) {
                let data = e.data
                let loginData = {
                    code: data.code,
                    userInfo: data.userInfo,
                    platform: 'wechat',
                    encryptedData: data.encryptedData,
                    iv: data.iv,
                    portrait: data.userInfo.avatarUrl,
                    user_name: data.userInfo.nickName,
                }
                this.imgUrl = data.userInfo.avatarUrl
                this.username = data.userInfo.nickName
                // this.handleOpend(data.code)

                this.$fetch.apiUser.getLogin({}, loginData)
                    .then(res => {

                        // console.log('login', res)
                        // console.log('token', res.data.data.fields.token)
                        uni.setStorageSync('token', res.data.data.fields.token)
                        // getApp().globalData.userId = 12

                        this.getMovieList()
                    })
                    .catch(err => {
                        console.error(err)
                    })
            } else {
                console.error('fail info', e)
            }
        },
        handleOpend (code) {
            wx.request({
                url: 'https://api.weixin.qq.com/sns/jscode2session',
                data: {
                    appid: 'wx45186691d4cf0676',
                    secret: '365d4d10142db58c632672e60a02f273',
                    grant_type: 'authorization_code',
                    js_code: code
                },
                method: 'GET',
                header: { 'content-type': 'application/json' },
                success: function (openIdRes) {
                    console.info("登录成功返回的openId：" + openIdRes.data.openid);
                },
                fail: function (error) {
                    console.info("获取用户openId失败");
                    console.info(error);
                }
            })
        },
        videoErrorCallback (e) {
            console.error('err', e)
            // uni.showModal({
            //     content: e.target.errMsg,
            //     showCancel: false
            // })
        },
        handleLike (e) {
            for (let o of this.vlogList) {
                if (o.id === e.id) {
                    o.is_like = true === e.is_like ? false : true
                    break;
                }
            }
        },
        handleCommit (e) {

        },
        handleShare (e) {

        },

    }
}

