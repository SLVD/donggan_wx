
export default {
    data () {
        return {
            pageData: {}, // 页面数据
            tempWidth: 0, // 模板宽度
            tempHeight: 0, // 模板高度
            vlogIndex: 0, // 当前激活的标签
            tagList: [], // 模板标签
            // imgData: ["http://tmp/wx46c78bb4f5f70fa7.o6zAJsylgK9HP121AvNl1f23HkLY.Ocqh7CxaEJ8156826aefbfbb78c3ec05c739a3938a4e.jpg"],  // 图片列表
            // imgData: new Array().fill(0),
            imgData: [], // 需要上传的文件数据
            fileUrlData: [], // 上传后的链接
            tempList: [], // 模板列表
            detail: {}, // 模板详情
            count: 0,
            temParams: { // 模板列表参数
                offset: 0,
                limit: 10,
                // catId: 1,
            },
            loadingText: ['加载中...', '已加载全部'], // 当前状态
            loadingStatus: -1 // -1, 0, 1
        }
    },
    onLoad (e) {
        // debugger
        console.log(this.$utils)
        if (!this.$utils.isEmpty(e)) {
            this.pageData = JSON.parse(e.data);
            this.imgData = this.pageData.imgData
        }

        this.$utils.checkAndLogin((e) => {
            if (e.flag) {
                let data = e.data
                let loginData = {
                    code: data.code,
                    userInfo: data.userInfo,
                    platform: 'wechat',
                    encryptedData: data.encryptedData,
                    iv: data.iv,
                    portrait: data.userInfo.avatarUrl,
                    user_name: data.userInfo.nickName,
                }

                this.$fetch.apiUser.getLogin({}, loginData)
                    .then(res => {
                        // console.log('login', res)
                        // console.log('token', res.data.data.fields.token)
                        uni.setStorageSync('token', res.data.data.fields.token)
                    })
                    .catch(err => {
                        console.error(err)
                    })
            } else {
                console.error('fail info', e)
            }
        })

        uni.showLoading({
            title: '加载中'
        })
        this.handleTemplateList()
        this.getTemplateTag()


    },
    onShow () {
        const { windowWidth, windowHeight } = uni.getSystemInfoSync();
        this.tempWidth = (windowWidth - 30) / 2
        this.tempHeight = this.tempWidth * this.$ratio
        console.log(windowWidth, windowHeight)
        console.log(this.tempWidth, this.tempHeight)

        this.initProvider()

    },
    onPullDownRefresh () {
        this.temParams.offset = 0
        this.handleTemplateList()
        setTimeout(() => {
            uni.stopPullDownRefresh();
        }, 1000);
    },
    onReachBottom () {
        this.temParams.offset++
        this.handleTemplateList()
    },
    onReachBottom () {
        if (this.temParams.offset * this.temParams.limit < this.count) {
            this.temParams.offset++
            this.loadingStatus = 0
            this.getMovieList()
        } else {
            this.loadingStatus = 1
            setTimeout(() => {
                this.loadingStatus = -1
            }, 1e3);
        }
    },
    methods: {
        initProvider() {
            const filters = ['weixin', 'qq', 'sinaweibo'];
            uni.getProvider({
                service: 'oauth',
                success: (res) => {
                    console.log('provider', res)
                    // if (res.provider && res.provider.length) {
                    //     for (let i = 0; i < res.provider.length; i++) {
                    //         if (~filters.indexOf(res.provider[i])) {
                    //             this.providerList.push({
                    //                 value: res.provider[i],
                    //                 image: '../../static/img/' + res.provider[i] + '.png'
                    //             });
                    //         }
                    //     }
                    //     this.hasProvider = true;
                    // }
                },
                fail: (err) => {
                    console.error('获取服务供应商失败：' + JSON.stringify(err));
                }
            });
        },
        handleSwitch (id) {
            this.vlogIndex = id
            this.temParams.offset = 0 // 还原页数为1
            this.handleTemplateList()
        },
        handleTemplateAuthorization () {
            uni.requestSubscribeMessage({
                tmplIds: [getApp().globalData.tempId],
                success: (res) => {
                    console.log('temp', red)
                },
                fail: (err) => {
                    console.log('temp', err)
                }
            })
        },
        handleTemplateList () {
            uni.showLoading({
                title: '加载中'
            })
            if (this.vlogIndex === 0) { // 代表全部模板
                delete this.temParams.catId
            } else {
                this.temParams.catId = this.vlogIndex
            }
            this.$fetch.apiMovie.templateList({}, this.temParams)
                .then(res => {
                    uni.hideLoading()
                    this.loadingStatus = -1
                    if (res.data.status === this.$T) {
                        // console.log('templateList', res)
                        // console.log('templateList', res.data.data.elements)
                        this.tempList = res.data.data.elements
                        this.count = res.data.data.fields
                    }
                })
                .catch(err => {
                    uni.hideLoading()
                    this.loadingStatus = -1
                    console.error(err);
                })
        },
        handleDetail (tplId) {
            uni.showLoading({
                title: '加载中'
            })
            this.$fetch.apiMovie.templateDetail({}, { tplId: tplId })
                .then(res => {
                    uni.hideLoading()
                    if (res.data.status === this.$T) {
                        // console.log('templateDetail', res)
                        // console.log('templateDetail', res.data.data.fields)
                        this.detail = res.data.data.fields
                        // this.tempList = res.data.data.elements
                    }
                })
                .catch(err => {
                    uni.hideLoading()
                    console.error(err);
                })
        },
        getTemplateTag () {
            this.$fetch.apiMovie.templateTag()
                .then(res => {
                    uni.hideLoading()
                    if (res.data.status === this.$T) {
                        // console.log('templateTag', res)
                        const data = res.data.data.elements
                        this.tagList = data
                        this.tagList.unshift({
                            id: 0,
                            title: '全部'
                        })
                        this.vlogIndex = data[0].id
                    }
                })
                .catch(err => {
                    uni.hideLoading()
                    console.error(err);
                })
        },
        getTemplateDetail (tplId) {
            uni.showLoading({
                title: '加载中'
            })
            this.$fetch.apiMovie.templateDetail({ tplId: tplId }, {})
                .then(res => {
                    uni.hideLoading()
                    if (res.data.status === this.$T) {
                        // console.log('templateDetail', res)

                    }
                })
                .catch(err => {
                    uni.hideLoading()
                    console.error(err);
                })
        },
        handleSubmit (data) {
            // this.handleTemplateAuthorization()
            // console.log(data)
            this.$utils.checkLogin((e) => {
                // if (e) {
                    this.$utils.pageSkip({
                        url: this.$path.upload_img,
                        price: data.price,
                        tplId: data.id
                    })
                // }
            })
        }
    }
}

