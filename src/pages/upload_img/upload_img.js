
const qiniuUploader = require("common/utils/qiniuUploader");
const jweixin = require('jweixin-module')  
export default {
    data () {
        return {
            pageData: {}, // 页面数据
            imgData: [], // 需要上传的文件数据
            fileUrlData: [], // 上传后的链接,

            uploadStatus: false, // 按钮的上传状态
            payBtn: ['去制作', '打赏制作'],
            payBtnIndex: 0,

            price: 0,
            tplId: 0

        }
    },
    onLoad (e) {
        // debugger
        if (!this.$utils.isEmpty(e)) {
            this.pageData = JSON.parse(e.data);
            this.imgData = this.pageData.imgData
            this.price = this.pageData.price
            this.tplId = this.pageData.tplId
            
            if (this.price === '免费') {
                this.payBtnIndex = 0
            } else {
                this.payBtnIndex = 1
            }

        }
    },
    onShow () {
        this.getUploadToken()

        console.log('jweixin', JSON.stringify(jweixin))
        alert(JSON.stringify(jweixin))

    },
    methods: {
        getUploadToken () {
            this.$fetch.apiUser.getQiniuToken()
                .then(res => {
                    // console.log('qiniu', res)
                    // console.log('qiniu-token', res.data.data.fields.token)
                    // console.log('qiniu-domain', res.data.data.fields.domain)
                    getApp().globalData.qiniuToken = res.data.data.fields.token
                    getApp().globalData.domain = res.data.data.fields.domain
                })
                .catch(err => {
                    console.error(err);
                })
        },
        handleChange (e) {  // 当前数组中发生改变时触发
            if (e.length > 0) {
                this.uploadStatus = true
            } else {
                this.uploadStatus = false
            }
            this.imgData = e
        },
        handleSubmit (file) {
            console.log('sese', this.imgData)
            if (this.imgData.length === 0) {
                uni.showToast({
                    title: '请添加图片',
                    icon: 'none'
                });
            } else {
                if (this.price === '免费') {
                    this.handleBatchUpload()
                } else {
                    this.handlePay(this.price)
                }
            }

            // this.handlePay(0.01)

        },
        handleClear () {
            this.imgData = []
            this.fileUrlData = []
            this.$refs.dkGridImg.handleClear()
        },
        handleBatchUpload () {
            for (let i = 0, len = this.imgData.length; i < len; i++) {
                uni.showLoading({
                    title: '上传中' + (i + 1) + '/' + len
                });
                this.handleUploadQiniu(this.imgData[i])
            }
        },
        handleSend (access_token) {
            uni.request({
                url: `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${access_token}`, //仅为示例，并非真实接口地址。
                method: "POST",
                data: {
                    "touser": "oVWNV4-EdjoE_PyX0VWtxMi6-zag",
                    "template_id": "dY8LfRiNhPEWu55i6omZrjrp0RbKBX1LkvcZLNji68Q",
                    "page": "main",
                    "data": {
                        "character_string1": {
                            "value": "339208499"
                        },
                        "thing2": {
                            "value": "2015年01月05日"
                        },
                    }
                },
                header: {
                    "custom-header": "hello" //自定义请求头信息
                },
                success: res => {
                    // console.log('send', res.data);
                }
            });
        },
        handleUploadQiniu (filePath) {
            qiniuUploader.upload(filePath, (res) => {
                // console.log('result ,', res);
                this.fileUrlData.push('https://' + res.imageURL) // 存储上传后的链接
                if (this.imgData.length === this.fileUrlData.length) { // 全部上传成功后触发 => 上传到服务器
                    this.handleUploadService()
                }
            }, (error) => {
                console.log('err =>', error)
                this.handleClear()
                this.imgData = []
                uni.showToast({
                    title: this.$tips.uploadQiniu,
                    icon: 'none',
                })
            }, {
                region: 'NCN',
                uptoken: getApp().globalData.qiniuToken,
                domain: getApp().globalData.domain,
            });
        },
        handleUploadService () {
            uni.hideLoading()
            // console.log('开始上传到服务器', this.fileUrlData);
            let params = {
                images: this.fileUrlData,
                tplId: this.tplId
            }
            this.$fetch.apiMovie.uploadImg({}, params)
                .then(res => {
                    // console.log('~~~~~upload service', res)
                    if (res.data.status === this.$T) {
                        // console.log(res)
                        uni.showToast({
                            title: this.$tips.uploadSuccess,
                        })
                        uni.switchTab({
                            url: this.$path.mine,
                        });
                    }
                })
                .catch(err => {
                    this.handleClear()
                    console.error(err);
                    uni.showToast({
                        title: this.$tips.uploadService,
                    })
                })
        },
        handlePay (price) {
            // console.log('pay price', price);
            let data = {
                "price": price * 100
            }
            this.$fetch.apiUser.pay({}, data)
                .then(res => {
                    // console.log(res)
                    // console.log(res.data.status)
                    if (res.data.status === this.$T) {
                        // console.log(res.data.data.fields.timeStamp)
                        // console.log(res.data.data.fields.nonceStr)
                        // console.log(res.data.data.fields.prepay_id)
                        // console.log(res.data.data.fields.paySign)
                        wx.requestPayment({
                            'timeStamp': res.data.data.fields.timeStamp,
                            'nonceStr': res.data.data.fields.nonceStr,
                            'package': 'prepay_id=' + res.data.data.fields.prepay_id,
                            'signType': "MD5",
                            'paySign': res.data.data.fields.paySign,
                            'success': (res) => {
                                wx.showToast({
                                    title: '支付成功',
                                    icon: 'success',
                                    duration: 2000,
                                    success: () => {
                                        this.handleBatchUpload()
                                    }
                                })
                            },
                            'fail': (res) => {
                                // this.imgData = []
                                wx.showToast({
                                    title: '支付失败',
                                    icon: 'none',
                                    duration: 2000,
                                    complete: () => {
                                        // console.log('pay fail')
                                    },
                                });

                            },
                            'complete': function (res) {

                            }
                        })
                    }
                })
                .catch(error => {

                })
        },

    }
}

