
import path from 'common/path/path'


const typeOf = function (obj) {
	const toString = Object.prototype.toString;
	const map = {
		'[object Boolean]': 'boolean',
		'[object Number]': 'number',
		'[object String]': 'string',
		'[object Function]': 'function',
		'[object Array]': 'array',
		'[object Date]': 'date',
		'[object RegExp]': 'regExp',
		'[object Undefined]': 'undefined',
		'[object Null]': 'null',
		'[object Object]': 'object'
	};
	return map[toString.call(obj)];
};


/*
 * 判断是不是空对象
 */
const isEmptyObject = function (val) {
	return typeOf(val) === 'object' && Object.keys(val).length === 0;
};

const isEmpty = function (obj) {
	return obj === undefined || obj === null || obj === "" || typeOf(obj) === 'array' && Object.keys(obj).length === 0 || typeOf(obj) === 'object' && Object.keys(obj).length === 0;
};


/**
 * 点击跳转页面
 */
const pageSkip = (params) => {
	let data = {
		...{
			url: '/pages/index/index',
		},
		...params
	};
	const url = data.url
	delete data.url
	uni.navigateTo({
		url: `${url}?data=${JSON.stringify(data)}`,
	});
};


/**
 * 解析页面数据
 * @param {*} e => json字符串
 */
const parsePageData = (e) => {
	// debugger
	if (isEmpty(e)) {
		return false
	} else {
		let pageData = JSON.parse(e.data);
		return pageData
	}
}


/**
 * 验证是否登录
 */
const checkLogin = (callback) => {
	if (!isEmpty(uni.getStorageSync('token'))) {
		callback(true)
	} else {
		wx.showModal({
			title: '提示',
			content: '请先登录',
			confirmText: '去登录',
			// showCancel: false,
			success: (res) => {
				if (res.confirm) {
					uni.reLaunch({
						url: path.mine,
					})
				}
				callback(false)
				// else if (res.cancel) {}
			}
		})
	}
}


/**
 * 验证并登录
 */
const checkAndLogin = (callback) => {
	let loginData = { // 登录数据
		data: {}, // 返回的信息
		flag: false, // 成功 或 失败
	}
	uni.login({
		provider: getApp().globalData.provider,
		success: (loginRes) => {
			// console.log('loginRes', loginRes)
			uni.getUserInfo({
				provider: getApp().globalData.provider,
				success: (infoRes) => {
					// console.log('infoRes', infoRes);
					uni.setStorageSync('userInfo', infoRes.userInfo)
					infoRes.code = loginRes.code
					infoRes.loginErrMsg = loginRes.errMsg
					loginData.data = infoRes
					loginData.flag = true
				},
				fail: (err) => {
					uni.setStorageSync('token', '')
					uni.setStorageSync('userInfo', '')
					loginData.data = err
					loginData.flag = false
					// console.error('信息获取失败', err)
				},
				complete: (res) => {
					callback(loginData)
				}
			});
		}
	});
}


export default {
	isEmptyObject,
	isEmpty,
	pageSkip,
	parsePageData,
	checkLogin,
	checkAndLogin,
	// deepCopy,
	// clickSkipPage,
};