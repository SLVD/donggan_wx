const Fly = require("flyio/dist/npm/wx")

// const api_prefix = '/api/' // api 前缀信息
const api_prefix = '' // api 前缀信息
// const api_prefix = '/api' // api 前缀信息

// 域名
import api_config from '../config/api';

export default function fetch(options) {
    return new Promise((resolve, reject) => {
        const instance = new Fly;
        instance.interceptors.request.use((config, promise) => {
            //给所有请求添加自定义header
            // debugger
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Nzg2MzUwOTMuNTU5MDM4LCJleHAiOjE1ODAxNDcwOTMuNTU5MDM4OSwibmlja05hbWUiOiJcdThkNzAiLCJnZW5kZXIiOjEsImxhbmd1YWdlIjoiemhfQ04iLCJjaXR5IjoiIiwicHJvdmluY2UiOiIiLCJjb3VudHJ5IjoiSWNlbGFuZCIsImF2YXRhclVybCI6Imh0dHBzOi8vd3gucWxvZ28uY24vbW1vcGVuL3ZpXzMyL3VmZDRSU2NsWU9LdldxNmNZcDJmV0piZ0NsYnppYWQ1ZlluaWJXR1ZhN0xxbGFzSElkNWRLajNOVEdqWFZpYkFwQXI4ZERSblQ5MUk0d2FsRTZka1FmM2dRLzEzMiIsImlkIjoxfQ._AYh2elZ5cBkRR6_RQBeWmniD1KvjSWWgdEYIDw83C8"
            config.headers["Content-Type"] = "application/json";
            config.headers["Authorization"] = uni.getStorageSync('token');
            // config.headers["Authorization"] = token;

            if (options.params && options.params.downloadVideo) {
                config.headers["Content-Type"] = "video/mpeg4";
            }

            return config;
        });
        instance.config.baseURL = api_config.api_host + api_prefix;
        // debugger
        if (options.params) {
            // console.log(options.params);
            let params = options.params;
            let url = options.url;
            for (let key in params) {
                url = url.replace(":" + key, params[key])
            };
            options.url = url;
            delete options.params;
            // console.log(options)
        };
        let data = null;
        if (options.data) {
            data = options.data;
        }
        instance.request(
            options.url,
            data, {
            method: options.method,
            timeout: 100000
        }).then((data => {
            resolve(data);
        })).catch((error) => {
                reject(error)
            })
    })
}
