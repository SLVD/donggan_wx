// key 与 页面名称相同
const prefix = '/pages'
export default {

    mine:  `${prefix}/mine/mine`,

    index: `${prefix}/index/index`,

    upload_img: `${prefix}/upload_img/upload_img`,

}